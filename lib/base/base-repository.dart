import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'base-response.dart';
enum RequestType { post, get, delete, put }

class BaseRepository {

  static const baseURL = "https://gitlab.com/-/snippets/2037796/raw/master";
  static const customUrl = "https://gitlab.com/Daffaal/data-json/-/raw/master";

  BuildContext context;
  setContext(BuildContext context) {
    this.context = context;
  }

  Future<dynamic> fetch(String path, RequestType type,{dynamic body, Map<String, String> header, String token, bool printLog = true}) async {
    var url = baseURL + "/" + path;
    print(url);
    if (body != null && printLog) print('body: $body');
    Map<String, String> headers;
    headers = {'content-type': "application/json"};
    var request = type == RequestType.get
        ? http.get(url, headers: headers)
        : type == RequestType.post
        ? http.post(url, headers: headers, body: (json.encode(body)))
        : type == RequestType.put
        ? http.put(url, headers: headers, body: (json.encode(body)))
        : http.delete(url, headers: headers);

    return request.then((http.Response response) async {
      final jsonBody = response.body;
      final statusCode = response.statusCode;
      if (printLog) print('respBody: ${response.body}');

      if (statusCode < 200 || statusCode >= 300 || jsonBody == null) {
        throw new Exception("$statusCode body: ${jsonBody != null ? jsonBody : "null"}");
      }

      final jsonMap = json.decode(jsonBody);
      final baseResponse = new BaseResponse.fromJson(jsonMap);

      return baseResponse.data;
    });
  }

  Future<dynamic> fetchCustom(String path, RequestType type,{dynamic body, Map<String, String> header, String token, bool printLog = true}) async {
    var url = customUrl + "/" + path;
    print(url);
    if (body != null && printLog) print('body: $body');
    Map<String, String> headers;
    headers = {'content-type': "application/json"};
    var request = type == RequestType.get
        ? http.get(url, headers: headers)
        : type == RequestType.post
        ? http.post(url, headers: headers, body: (json.encode(body)))
        : type == RequestType.put
        ? http.put(url, headers: headers, body: (json.encode(body)))
        : http.delete(url, headers: headers);

    return request.then((http.Response response) async {
      final jsonBody = response.body;
      final statusCode = response.statusCode;
      if (printLog) print('respBody: ${response.body}');

      if (statusCode < 200 || statusCode >= 300 || jsonBody == null) {
        throw new Exception("$statusCode body: ${jsonBody != null ? jsonBody : "null"}");
      }

      final jsonMap = json.decode(jsonBody);
      final baseResponse = new BaseResponse.fromJson(jsonMap);

      return baseResponse.data;
    });
  }
}